import remarkPresetLintConsistent from 'remark-preset-lint-consistent'
import remarkPresetLintRecommended from 'remark-preset-lint-recommended'
import remarkToc from 'remark-toc'

const remarkConfig = {
  settings: {
    bullet: '*', // Use `*` for list item bullets (default)
		listItemIndent: 'one', // Match the linting
    // See <https://github.com/remarkjs/remark/tree/main/packages/remark-stringify> for more options.
  },
  plugins: [
		// Automatically insert a table of content before the linting
		[remarkToc, {tight: true}],
		// Lint the markdown
    remarkPresetLintConsistent, // Check that markdown is consistent.
    remarkPresetLintRecommended, // Few recommended rules.
    // Generate a table of contents in `## Contents`
		// `remark-lint-list-item-indent` is configured with `tab-size` in the
		// recommended preset, but if we’d prefer something else, it can be
		// reconfigured:
		["remark-lint-list-item-indent", "space"],
  ]
}

export default remarkConfig