import { babel } from '@rollup/plugin-babel';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';

export default [
  // An ESM build that can be loaded in the browser with `<script type="module">`
  // or used through your favourite bundler (not tree-shakable, though)
  {
    input: 'src/bundled-api.js',
    output: [{ file: `dist/${pkg.name}.js`, format: 'es' }],
  },
  // A minified ESM build to make loading quicker for `<script type="module">`
  {
    input: 'src/bundled-api.js',
    output: [
      {
        file: `dist/${pkg.name}.min.js`,
        format: 'es',
        sourcemap: true,
      },
    ],
    plugins: [terser()],
  },

  // An ES5 UMD build for supporting older browsers
  // If necessary to bundle other modules than `index`
  // create a separate `src/umd.js` that exports what's needed
  {
    input: 'src/bundled-api.js',
    output: {
      name: pkg.name,
      file: `dist/${pkg.name}.umd.js`,
      format: 'umd',
    },
    plugins: [babel({ babelHelpers: 'bundled' })],
  },
  // And its minified counterpart
  {
    input: 'src/bundled-api.js',
    output: {
      name: pkg.name,
      file: `dist/${pkg.name}.umd.min.js`,
      format: 'umd',
      sourcemap: true,
    },
    plugins: [babel({ babelHelpers: 'bundled' }), terser()],
  },
];
