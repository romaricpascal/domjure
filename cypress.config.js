import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    specPattern: '{src,cypress}/**/*.cy.js',
    supportFile: false,
  },
});
