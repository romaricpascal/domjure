import chai, {expect} from 'chai';
import sinonChai from 'sinon-chai';
import Window from 'window';
chai.use(sinonChai);

// Export window as a global as createElement requires it internally and it
// doesn't really make sense to inject it as an option
global.window = new Window();
