/**
 * Entry point for rollup to bundle the whole library
 * in a single file, for UMD import or should users
 * want to load a single module with no tree-shaking in their browser
 */
export * as dom from './dom.js';
export * from './index.js';
