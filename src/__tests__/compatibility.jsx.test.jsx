/** @jsx createElement */
import { expect } from 'chai';
import htm from 'htm';
// The variable is used by the JSX pragma
// eslint-disable-next-line no-unused-vars
import { createElement, NAMESPACE_URI_SVG } from '../index.js';

describe('domjure', function () {
  describe('jsx', function () {
    describe('createElement', function () {
      it('is JSX compatible', function () {
        const classes = 'text-xl';
        const destructured = {
          'data-component': 'something',
        };
        const element = (
          <div id="an-id" class={classes} {...destructured}>
            <h1>
              A heading <span>with HTML</span>
            </h1>
            <p>A paragraph</p>
          </div>
        );

        expect(element.outerHTML).to.equal(
          `<div id="an-id" class="text-xl" data-component="something"><h1>A heading <span>with HTML</span></h1><p>A paragraph</p></div>`,
        );
      });

      it('creates SVG elements in JSX', function () {
        const element = <path namespaceURI={NAMESPACE_URI_SVG} />;

        expect(element).to.be.an.instanceOf(window.SVGElement);
      });
    });
  });

  describe('htm', function () {
    let html;

    before(function () {
      html = htm.bind(createElement);
    });

    it('is htm compatible', function () {
      const classes = 'text-xl';
      const destructured = {
        'data-component': 'something',
      };
      const element = html`<div id="an-id" class=${classes} ...${destructured}>
        <h1>A heading <span>with HTML</span></h1>
        <p>A paragraph</p>
      </div>`;

      expect(element.outerHTML).to.equal(
        `<div id="an-id" class="text-xl" data-component="something"><h1>A heading <span>with HTML</span></h1><p>A paragraph</p></div>`,
      );
    });
  });
});
