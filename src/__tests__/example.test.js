import { expect } from 'chai';
import { addToElement, createElement, setOnElement } from '../index.js';
import { setAttributes, setStyle } from '../dom.js';

describe('examples', function () {
  describe('custom set on element', function () {
    it('works as expected', function () {
      const customSetters = {
        // Abstracts the attributes necessary to make
        // the element a toggle for a Bootstrap modal
        bootstrapModalToggle(element, name, value) {
          // You could add a check that the element is actually
          // a button as well
          setAttributes(
            element,
            {
              toggle: 'modal',
              target: `#${value}`,
            },
            'data-bs-',
          );
        },
        // Provides a shorthand for setting aria attributes
        aria(element, name, value) {
          setAttributes(element, value, 'aria-');
        },
        // Provide support for setting the element styles as both string or object
        style(element, name, value) {
          setStyle(element, value);
        },
        // Provide a shorthand for setting data attributes
        data(element, name, value) {
          setAttributes(element, value, 'data-');
        },
      };

      function customSetOnElement(element, name, ...args) {
        if (Reflect.has(customSetters, name)) {
          return customSetters[name](element, name, ...args);
        }

        // Fallback to the default implementation if none is found
        return setOnElement(element, name, ...args);
      }

      const customCreateElement = createElement.bind({
        setOnElement: customSetOnElement,
      });

      const result = customCreateElement('button', {
        bootstrapModalToggle: 'id-of-a-modal',
        aria: {
          disabled: true,
        },
        style: {
          // Ideally that'd be used for setting custom properties
          // but JSDom doesn't support them, so testing with a regular property
          // https://github.com/jsdom/jsdom/issues/1895
          'line-height': '1.25em',
        },
        data: {
          'app-ignore-ctrl-click': '',
        },
        // This will fallthrough to the library's default implementation
        type: 'button',
      });

      expect(result.getAttribute('data-bs-toggle')).to.equal('modal');
      expect(result.getAttribute('data-bs-target')).to.equal('#id-of-a-modal');
      expect(result.getAttribute('aria-disabled')).to.equal('true');
      expect(result.getAttribute('data-app-ignore-ctrl-click')).to.equal('');
      expect(result.getAttribute('style')).to.equal('line-height: 1.25em;');
      expect(result.getAttribute('type')).to.equal('button');
    });
  });
  describe('custom add to element', function () {
    it('works as expected', function () {
      function customAddToElement(element, child, index, ...args) {
        // Clean up the current HTML before inserting the first node
        if (index == 0) {
          element.textContent = '';
        }

        // Allow to create elements from an object structure
        if (typeof child == 'object' && !(child instanceof window.Node)) {
          element.appendChild(
            customCreateElement(
              child.tagName,
              child.attributes,
              ...(child.children || []),
            ),
          );
        } else {
          addToElement(element, child, ...args);
        }
      }

      const customCreateElement = createElement.bind({
        addToElement: customAddToElement,
      });

      // Create an element with existing content to check the cleanup
      const existingElement = window.document.createElement('article');
      existingElement.innerHTML = '<p>Some content already in there</p>';

      const article = customCreateElement(
        existingElement,
        null,
        // This'll be using the custom element creation
        {
          tagName: 'h1',
          children: 'The title of the article',
        },
        // This will fall through too the default implementation
        '<p>Some content, from a CMS, for ex.</p>',
      );

      expect(article.outerHTML).to.equal(
        '<article><h1>The title of the article</h1><p>Some content, from a CMS, for ex.</p></article>',
      );
    });
  });
});
