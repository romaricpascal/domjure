import { expect } from 'chai';
import { setAttributes, setDataset, setStyle } from '../dom.js';

describe('dom', function () {
  let element;
  beforeEach(function () {
    element = window.document.createElement('div');
  });

  describe('setAttributes', function () {
    it('sets multiple attributes on the element', function () {
      setAttributes(element, {
        id: 'hello',
        class: 'text-xl',
        'data-test': 'value',
      });
      expect(element.outerHTML).to.equal(
        '<div id="hello" class="text-xl" data-test="value"></div>',
      );
    });

    it('ignores false-y list of attributes', function () {
      // Test checks that none of these throw
      setAttributes(element);
      setAttributes(element, null);
      setAttributes(element, false);
      setAttributes(element, '');
      expect(element.outerHTML).to.equal('<div></div>');
    });

    it('prefixes the attribute names', function () {
      setAttributes(element, { a: 1, b: 2 }, 'data-');
      expect(element.outerHTML).to.equal('<div data-a="1" data-b="2"></div>');
    });
  });

  describe('setStyle', function () {
    it('sets styles as a string', function () {
      setStyle(
        element,
        'background-color: black; color: white; padding-block: 1rem;',
      );
      expect(element.outerHTML).to.equal(
        '<div style="background-color: black; color: white; padding-block: 1rem;"></div>',
      );
    });
    it('sets styles as individual properties', function () {
      setStyle(element, {
        backgroundColor: 'black',
        color: 'white',
        'padding-block': '1rem',
      });
      expect(element.outerHTML).to.equal(
        '<div style="background-color: black; color: white; padding-block: 1rem;"></div>',
      );
    });
  });

  describe('setDataset', function () {
    it('sets the dataset of the element', function () {
      setDataset(element, {
        a: 1,
        bC: 2,
      });
      expect(element.outerHTML).to.equal('<div data-a="1" data-b-c="2"></div>');
    });
  });
});
