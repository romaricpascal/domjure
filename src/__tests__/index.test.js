import { expect } from 'chai';
import { spy } from 'sinon';
import { createElement, NAMESPACE_URI_SVG } from '../index.js';
import { whileSpyingOn } from './sinon.js';

describe('domjure', function () {
  describe('createElement', function () {
    describe('element creation', function () {
      it('creates the relevant element', function () {
        const element = createElement('header');

        expect(element).to.be.an.instanceOf(window.HTMLElement);
        expect(element.tagName).to.equal('HEADER');
      });

      it('creates the element in the requested namespace', function () {
        const element = createElement('path', {
          namespaceURI: NAMESPACE_URI_SVG,
        });
        expect(element).to.be.an.instanceOf(window.SVGElement);
        expect(element.tagName).to.equal('path'); //SVG doesn't uppercase its tag name
      });
      it('returns undefined if argument is falsey', function () {
        expect(createElement('')).to.be.undefined;
        expect(createElement(false)).to.be.undefined;
        expect(createElement(null)).to.be.undefined;
        expect(createElement()).to.be.undefined;
      });
      it('returns the passed element if the argument is already an element', function () {
        const existingElement = window.document.createElement('div');
        expect(createElement(existingElement)).to.equal(existingElement);
      });
    });

    describe('content', function () {
      it('set a single string as HTML', function () {
        const element = createElement(
          'div',
          null,
          '<h1>A heading</h1><p>A paragraph</p>',
        );
        expect(element.children[0].outerHTML).to.equal('<h1>A heading</h1>');
        expect(element.children[1].outerHTML).to.equal('<p>A paragraph</p>');
      });
      it('appends a single node to the element', function () {
        const element = createElement(
          'div',
          null,
          window.document.createElement('hr'),
        );

        expect(element.children[0].outerHTML).to.equal('<hr>');
      });
      it('walks an array and appends text and nodes in order', function () {
        const element = createElement('div', null, [
          window.document.createElement('hr'),
          null,
          false,
          undefined,
          '', // If an empty string is really necessary, use createTextNode
          window.document.createElement('br'),
        ]);

        expect(element.children[0].outerHTML).to.equal('<hr>');
        expect(element.children[1].outerHTML).to.equal('<br>');
      });

      it('collects rest of arguments and treats them as an array', function () {
        const element = createElement(
          'div',
          null,
          window.document.createElement('hr'),
          null,
          false,
          undefined,
          '', // If an empty string is really necessary, use createTextNode
          window.document.createElement('br'),
        );

        expect(element.children[0].outerHTML).to.equal('<hr>');
        expect(element.children[1].outerHTML).to.equal('<br>');
      });

      it('treats option as a child if a string', function () {
        const element = createElement('p', 'A paragraph');

        expect(element.outerHTML).to.equal('<p>A paragraph</p>');
      });

      it('treats option as a child if a Node', function () {
        const element = createElement('p', window.document.createElement('br'));

        expect(element.outerHTML).to.equal('<p><br></p>');
      });

      it('treats option as a child if an Array', function () {
        const element = createElement('div', [
          window.document.createElement('hr'),
          null,
          false,
          undefined,
          '', // If an empty string is really necessary, use createTextNode
          window.document.createElement('br'),
        ]);

        expect(element.children[0].outerHTML).to.equal('<hr>');
        expect(element.children[1].outerHTML).to.equal('<br>');
      });
    });

    describe('properties', function () {
      it('sets the properties on the element if the element supports it', function () {
        whileSpyingOn(
          window.HTMLAnchorElement.prototype,
          'href',
          function (set) {
            const element = createElement('a', { href: '/' }, 'Home');

            expect(set).to.have.been.calledWith('/');

            expect(element.outerHTML).to.equal('<a href="/">Home</a>');
          },
        );
      });
      it('sets the attribute on the element if a property exists but is readonly', function () {
        // `dataset` is a property that will be on the HTMLElement
        // but cannot be set as it's only a getter
        const element = createElement('a', { dataset: true });

        expect(element.outerHTML).to.equal('<a dataset="true"></a>');
      });
      it('sets the attribute on the element if the property does not exist', function () {
        const heading = createElement(
          'h1',
          { class: 'text-xl', 'data-linkable': 'false' },
          'Welcome',
        );
        expect(heading.outerHTML).to.equal(
          '<h1 class="text-xl" data-linkable="false">Welcome</h1>',
        );

        const label = createElement(
          'label',
          { for: 'field-id' },
          'Description',
        );
        expect(label.outerHTML).to.equal(
          '<label for="field-id">Description</label>',
        );
      });
    });
  });

  describe('getElementCreator', function () {
    it('allows customisation of content addition as a function', function () {
      // A custom element creator that will create new elements
      // based based on the object properties
      // eslint-disable-next-line no-shadow
      const addToElement = spy();

      // A spy on customCreateElement to check that it is the one actually invoked
      const customCreateElement = createElement.bind({ addToElement });

      // Call the custom createElement function with a two level deep
      // tree of elements, so that we check that the second invocation
      // also does use the custom `addToElement` for creating the objects
      const children = ['Hello', 'World'];
      const element = customCreateElement('div', null, children);

      // Assert that the configured `addToElement` did receive the expected arguments
      expect(addToElement).to.have.been.calledWith(
        element,
        'Hello',
        0,
        children,
      );
      expect(addToElement).to.have.been.calledWith(
        element,
        'World',
        1,
        children,
      );
    });

    it('allows customisation of settings application as a function', function () {
      const setOnElement = spy();

      const customCreateElement = createElement.bind({ setOnElement });

      const heading = customCreateElement(
        'h1',
        { class: 'text-xl', 'data-linkable': 'false' },
        'Welcome',
      );

      expect(setOnElement).to.have.been.calledWith(heading, 'class', 'text-xl');
      expect(setOnElement).to.have.been.calledWith(
        heading,
        'data-linkable',
        'false',
      );
    });
  });
});
