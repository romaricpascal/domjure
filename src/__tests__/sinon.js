import { spy } from 'sinon';

/**
 * Calls the given function with a spy of the setter
 * of given property name on the given object,
 * ensuring things get cleaned up after
 *
 * Syntactic sugar around sinon.spy
 * https://sinonjs.org/releases/latest/spies/#using-a-spy-to-wrap-an-existing-method
 *
 * @param {any} object - The object to spy on
 * @param {string | symbol} propertyName - The name of the property on whose setter to spy
 * @param {Function} fn - The test to wrap
 * @returns {*} - Whatever the test returns
 */
export function whileSpyingOn(object, propertyName, fn) {
  const { set } = spy(object, propertyName, ['set']);

  try {
    return fn(set);
  } finally {
    set.restore();
  }
}
