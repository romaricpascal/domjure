// Main ESM file
import { appendNode, appendHtml } from './dom.js';

/**
 *Creates an element with the given properties or attributes, and children.
 * Can also accept an existing element to set its properties or attributes.
 *
 * @param {string | Element} tagNameOrElement - The tagName of the element to create or an existing element
 * @param {Object<string, any>} propertiesOrAttributes - The properties or attributes to set
 * @param {...(string | Element)} children - HTML or elements to append as content
 * @returns {Element} The created or provided element, with set properties, attributes and content
 */
export function createElement(
  tagNameOrElement,
  propertiesOrAttributes,
  ...children
) {
  if (!tagNameOrElement) {
    return;
  }

  // `createElement(tagName, ...children)`
  if (
    Array.isArray(propertiesOrAttributes) ||
    typeof propertiesOrAttributes == 'string' ||
    propertiesOrAttributes instanceof window.Node
  ) {
    children.unshift(propertiesOrAttributes);
    propertiesOrAttributes = {};
  }

  // `createElement(tagName, [child1, child2])`
  // `createElement(tagName,settings, [child1, child2])`
  if (children.length == 1 && Array.isArray(children[0])) {
    children = children[0];
  }

  const { namespaceURI, ...settings } = propertiesOrAttributes || {};

  const element = createBlankElement(tagNameOrElement, namespaceURI);

  const doAddToElement = (this && this.addToElement) || addToElement;

  // No need to start a loop if there's no children
  if (children.length) {
    // `for...in` would provide string indices rather than numbers
    // so going with a good ol' `for`
    for (var index = 0; index < children.length; index++) {
      doAddToElement(element, children[index], index, children);
    }
  }

  const doSetOnElement = (this && this.setOnElement) || setOnElement;
  for (const settingName in settings) {
    doSetOnElement(element, settingName, settings[settingName], settings);
  }

  return element;
}

// Namespace URI allowing to create non-HTML elements
// https://developer.mozilla.org/en-US/docs/Web/API/Document/createElementNS#important_namespace_uris
/**
 * Namespace for creating HTML elements
 *
 * @type {string}
 */
export const NAMESPACE_URI_HTML = 'http://www.w3.org/1999/xhtml';
/**
 * Namespace for creating SVG elements
 *
 * @type {string}
 */
export const NAMESPACE_URI_SVG = 'http://www.w3.org/2000/svg';
/**
 * Namespace for creating MathML elements
 *
 * @type {string}
 */
export const NAMESPACE_URI_MATHML = 'http://www.w3.org/1998/Math/MathML';

/**
 * Creates an element (in the relevant namespace)
 *
 * @param {string | Element} tagNameOrElement - The tagName of the element to
 * create. For convenience, if you pass an element, the function will just
 * return it.
 * @param {string} [namespaceURI=NAMESPACE_URI_HTML] - The namespace of the element to create
 * @returns {Element} - The created element, or passed element if `tagNameOrElement` was an `Element`
 */
export function createBlankElement(
  tagNameOrElement,
  namespaceURI = NAMESPACE_URI_HTML,
) {
  if (tagNameOrElement instanceof window.Element) {
    return tagNameOrElement;
  }

  return window.document.createElementNS(namespaceURI, tagNameOrElement);
}

/**
 * @callback setOnElement
 * @param {Element} element - The element to set the value on
 * @param {string} name - The name of what to set
 * @param {any} value - The value to set
 * @param {Object<string,any>} [attributes] - The attributes being set on the object
 */

/**
 * Default setOnElement implementation
 *
 * Sets the value as a property if the element already has this property
 * or as an attribute otherwise
 *
 * @param {Element} element - The element to set the value on
 * @param {string} name - The name of what to set
 * @param {any} value - The value to set
 */
export function setOnElement(element, name, value) {
  // Try to set the property on the element if:
  // - it already exists on the element
  // - it can actually be set (`Reflect.set` will return false
  //   if it doesn't succeed setting the property)
  if (!(Reflect.has(element, name) && Reflect.set(element, name, value))) {
    element.setAttribute(name.toString(), value);
  }
}

/**
 * @callback addToElement
 * @param {Element} element - The element to add children to
 * @param {any} child - The child to add to the element
 * @param {number} [index] - The index of the child in the list of children
 * @param {Array} children - The list of children being added to the element
 */

/**
 * Default implementation of addToElement
 *
 * Appends the element as a DOM Node if it already is one,
 * otherwise as HTML.
 *
 * @param {Element} element - The element to add children to
 * @param {any} child - The child to add to the element
 */
export function addToElement(element, child) {
  if (child instanceof window.Node) {
    appendNode(element, child);
  } else {
    appendHtml(element, child);
  }
}
