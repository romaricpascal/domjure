/**
 * Sets an attribute on the given element
 *
 * @param {Element} element - The element to set the attribute on
 * @param {string} attributeName - The name of the attribute
 * @param {*} value - The value of the attribute
 */
export function setAttribute(element, attributeName, value) {
  element.setAttribute(attributeName, value);
}

/**
 * @param {Element} element - The element to set the attributes on
 * @param {Object<string,string>} attributes - A hash of the attributes to set on the element
 * @param {string} [prefix=''] - An optional prefix that will be prepended to each attribute name
 */
export function setAttributes(element, attributes, prefix = '') {
  for (const attributeName in attributes) {
    element.setAttribute(prefix + attributeName, attributes[attributeName]);
  }
}

/**
 * Sets the style of the given element
 *
 * @param {HTMLElement|SVGElement} element - The element to set the style of
 * @param {string|Object<string,string>} style - The style to set, either as a CSS string or an Object associating property names with their values
 */
export function setStyle(element, style) {
  if (typeof style == 'string') {
    element.style = style;
  } else {
    for (const propertyName in style) {
      if (propertyName.indexOf('-') !== -1) {
        element.style.setProperty(propertyName, style[propertyName]);
      } else {
        element.style[propertyName] = style[propertyName];
      }
    }
  }
}

/**
 * Assigns the given dataset to the element
 *
 * @param {HTMLElement|SVGElement} element - The element to assign the dataset to
 * @param {Object<string,string>} dataset - The dataset to assign
 */
export function setDataset(element, dataset) {
  Object.assign(element.dataset, dataset);
}

/**
 * @param {Node} element - The node to append the child to
 * @param {Node} child - The child to append
 */
export function appendNode(element, child) {
  element.appendChild(child);
}

/**
 * @param {Element} element - The Element to append the HTML to
 * @param {string} html - The HTML to append
 */
export function appendHtml(element, html) {
  element.insertAdjacentHTML('beforeend', html);
}
