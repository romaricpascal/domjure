describe('domjure', () => {
  beforeEach(function () {
    cy.visit(`cypress/fixtures/blank.html`);
  });

  it(
    'works in the browser',
    withWindow(({ document, library }) => {
      document.body.appendChild(library.createElement('hr'));
      expect(document.querySelector('hr')).not.to.be.undefined;
    }),
  );
});

function withWindow(fn) {
  return function () {
    cy.window().then(fn);
  };
}
